import React, { Component } from 'react'

import { Link, withRouter } from 'react-router-dom'

class Header extends Component {
	render() {
		return (
			<div>
				<h1>{this.props.title}</h1>
				{this.props.history.location.pathname !== "/" ? (
					<Link to="/">Вернуться к списку проектов</Link>
				) : null}
			</div>
		)
	}
}

export default withRouter(Header)