import React from "react"

import {addTask} from '../api/tasks'

export default function TaskForm(props) {
	const [completed, setCompleted] = React.useState(false);
	const [content,   setContent]   = React.useState('');
	const [date,      setDate]      = React.useState((new Date()).toJSON().slice(0, 10));
	const [priority,  setPriority]  = React.useState(1);
	
	const completedChanged = e => {
		setCompleted(e.target.checked);
	}

	const contentChanged = e => {
		setContent(e.target.value)
	}
	
	const dateChanged = e => {
		setDate(e.target.value);
	}
	
	const prioritySelected = e => {
		setPriority(e.target.value);
	}

	const handleSubmit = e => {
		e.preventDefault()
		addTask(props.projectId, {
			completed,
			content,
			date,
			priority
		}).then(res => {
			console.info({ res })
			props.onTaskAdded && props.onTaskAdded(res)
		})
	}

	return (
		<div
			style={{
				width: "300px",
				height: "300px",
				border: "1px solid #333",
				padding: "10px"
			}}
		>
			<h3>Новая задача</h3>
			<form onSubmit={handleSubmit}>
				<p>
					<label htmlFor="newTaskContent">Цель: </label>
					<input id="newTaskContent" type="text" value={content} onChange={contentChanged} />
				</p>
				<p>
					<label htmlFor="newTaskDate">Срок: </label>
					<input id="newTaskDate" type="date" value={date} onChange={dateChanged} />
				</p>
				<p>
					<label htmlFor="newTaskPriority">Приоритет: </label>
					<select id="newTaskPriority" defaultValue={priority} onChange={prioritySelected}>
						<option value="1">Нормально</option>
						<option value="2">Важно</option>
						<option value="3">Срочно</option>
					</select>
				</p>
				<p>
					<input id="newTaskCompleted" type="checkbox" defaultChecked={completed} onChange={completedChanged} />
					<label htmlFor="newTaskCompleted"> завершена</label>
				</p>
				<p>
					<button type="submit">Добавить</button>
				</p>
			</form>
		</div>
	)
}
